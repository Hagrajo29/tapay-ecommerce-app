

import { Fragment } from 'react';
import AppNavbar from './components/AppNavbar';
import { Container } from 'react-bootstrap';
//import Login from './pages/Login';
//import Register from './pages/Register';
import Products from './pages/Products';
import './App.css';

function App() {
  return (
    <Fragment>
      <AppNavbar />
      <Container>
          <Products />
      </Container>
    </Fragment>
  );
}

export default App;
