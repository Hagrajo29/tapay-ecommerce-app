import { Form, Button } from 'react-bootstrap';

	export default function Register() {

		return (
		    <Form>

		    	<Form.Group controlId="userName">
		            <Form.Label>Username</Form.Label>
		            <Form.Control 
			            type="userName" 
			            placeholder="Username" 
			            required
		            />
		        </Form.Group>
		        <Form.Group controlId="userEmail">
		            <Form.Label>Email Address</Form.Label>
		            <Form.Control 
			            type="email" 
			            placeholder="Enter Email" 
			            required
		            />
		            <Form.Text className="text-muted">
		                No email sharing.
		            </Form.Text>
		        </Form.Group>

		        <Form.Group controlId="password1">
		            <Form.Label>Password</Form.Label>
		            <Form.Control 
			            type="password" 
			            placeholder="Password" 
			            required
		            />
		        </Form.Group>

		        <Form.Group controlId="password2">
		            <Form.Label>Verify Password</Form.Label>
		            <Form.Control 
			            type="password" 
			            placeholder="Verify Password" 
			            required
		            />
		        </Form.Group>

		        <Button variant="primary" type="submit" id="submitBtn">
		            Submit
		        </Button>
		    </Form>
		)

	}
