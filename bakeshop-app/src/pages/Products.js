import { Fragment } from 'react';
import ProductCard from '../components/ProductCard';
import productsData from '../productsData/productsData';

export default function Products() {
	console.log(productsData);
	console.log(productsData[0]);

	const products = productsData.map(product =>{
		return (
			<ProductCard key = {product.id} productProp={product}/>
			);
	})
	return(
		<Fragment>
			{products}
		</Fragment>
	)
}
