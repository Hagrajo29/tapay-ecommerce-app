import { Form, Button } from 'react-bootstrap';

	export default function Login() {

		return (
		    <Form>
		        <Form.Group controlId="userEmail">
		            <Form.Label>Email Address</Form.Label>
		            <Form.Control 
		                type="email" 
		                placeholder="Enter Email" 
		                required
		            />
		        </Form.Group>

		        <Form.Group controlId="password">
		            <Form.Label>Password</Form.Label>
		            <Form.Control 
		                type="password" 
		                placeholder="Password" 
		                required
		                />
		        </Form.Group>

		        <Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		        </Button>
		  	</Form>
		)
	}
