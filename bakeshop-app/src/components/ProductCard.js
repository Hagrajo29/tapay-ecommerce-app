
import { Col, Row, Card, Button } from 'react-bootstrap';

export default function ProductCard(props) {
	console.log(props);
	return (
		<Row xs={1} md={2} className="g-4">
  			{Array.from({ length: 4 }).map((_, idx) => (
    			<Col>
      				<Card>
      					<Card.Img variant="top" src="holder.js/100px160" />
        				<Card.Body>
          					<Card.Title>Name</Card.Title>
          					<Card.Subtitle>Description:</Card.Subtitle>
          					<Card.Text>
            				This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
          					</Card.Text>
          					<Card.Subtitle>Price:</Card.Subtitle>
          					<Card.Text>PhP 400</Card.Text>
          					<Button variant="primary">Add to Cart</Button>
        				</Card.Body>
      				</Card>
    			</Col>
 			 ))}
		</Row>        
	)
}
