import { Component } from 'react';
// React Boostrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default class AppNavbar extends Component {
	render() {
		return (
			<Navbar bg="info" expand="lg">
			    <Navbar.Brand href="#home">Fairytail Cakes & Pastries</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
			        <Nav className="ml-auto">
			            <Nav.Link href="#home">Home</Nav.Link>
			            <Nav.Link href="#register">Register</Nav.Link>
			            <Nav.Link href="#login">Login</Nav.Link>
			        </Nav>
			    </Navbar.Collapse>
			</Navbar>
		)
	}
}